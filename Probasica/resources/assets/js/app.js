
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/Example.vue'));
Vue.component('pagination',require('./components/posts.vue'));
Vue.component('mon-composant',require('./components/more.vue'));
//Vue.component('paginate',require('./components/pagination.vue'));


const app = new Vue({
    el: '#app',
    data:{
      elements:[],
      tabName:[],
      offset:6,a
      show:true,
      showList:false,
      mois: ["janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre"],
    },
    methods: {
      pagination:function(event){
          var url = event.target.href ;
          var reference=[];
          axios.get(url).then((reponse)=>{
         $('.blog-post').fadeOut(1000);
            var text = $(event.target).parents('.pagination').find('li.active').text();
           var newUrl = url.substr(0,url.length-1 );
            newUrl = newUrl+text;
            $(event.target).parents('.pagination').find('li.active').html('<a href='+newUrl+'>'+text+'</a>');
            $(event.target).parents('.pagination').find('li.active').removeClass('active');
           $(event.target).closest('li')[0].classList.add('active');
            var spanText = $('.pagination li.active').text();
            $('.pagination li.active').html('<span>'+spanText+'</span>');
              var fleche = $('.pagination .pagination').find('li:first').text();
            if(reponse.data.current_page == reponse.data.last_page){
              var premier = url.substr(0,url.length-1 );
              premier = premier+1;
             $('.pagination .pagination').find('li:last').addClass('disabled');
                $('.pagination .pagination').find('li:first').removeClass('disabled');
                $('.pagination .pagination').find('li:first').html('<a href='+premier+'>'+fleche+'</a>');


            }else{
               $('.pagination .pagination').find('li:last').removeClass('disabled');
               $('.pagination .pagination').find('li').first().addClass('disabled');

            }

          //  console.log(reponse.data);
           var posts = reponse.data.posts.data;

           //console.log(reponse.data.posts);
            posts.forEach((element)=>{
              var date = element.created_at.substr(0,10);

              element.created_at = this.dateFr(date);


              var newsText = element.text.substr(0,100);
              element.text= newsText+'...';
              this.elements.push(element);
            })
            //this.show = false;
        }).catch(()=>{
          alert("erreur lors de la pagination");
        })
      },
      dateFr : function(date){
        var annee =date.substr(0,4);
        var mois = parseInt(date.substr(5,2));
        var jour = date.substr(8,2);
        return jour+" "+this.mois[mois-1]+" "+annee;
      },
      more:function(){
        axios.post('portfolio/more',{
          offset : this.offset
        }).then((reponse)=>{
          //recuperation des donnes ajax
          var tableau = reponse.data.works;
          tableau.forEach((element)=>{
              element.slug = "portfolio/"+element.slug;
              this.tabName.push(element);
          })

          this.show = true;
          this.showList = true;
          this.offset += 6;

        }).catch(()=>{
          // alert en cas d'eurreur de la requette
          alert('erreur lors du chargement des travaux ');
        })

      },
    }
});
