@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src={{ asset('/uploads/img/user/'.Auth::user()->avatar) }} class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            <a href="{{ url('/') }}"><i class="fa fa-home"></i> <span>Partie publique</span></a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">{{ trans('backpack::base.administration') }}</li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
          <!-- <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/elfinder') }}"><i class="fa fa-upload"></i> <span>Gestion des fichiers</span></a></li>-->
          <li><a href="{{ url('admin/posts') }}"><i class="fa fa-book"></i> <span>Gestion des articles</span></a></li>
          <li><a href="{{ url('admin/categories') }}"><i class="fa fa-bookmark"></i> <span>Gestion des catégories</span></a></li>
          <li><a href="{{ url('admin/works') }}"><i class="fa fa-briefcase"></i> <span>Gestion des travaux</span></a></li>
          <li><a href="{{ url('admin/tags') }}"><i class="fa fa-tags"></i> <span>Gestion des tags</span></a></li>
          <!-- ======================================= -->
          <li class="header">{{ trans('backpack::base.user') }}</li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
