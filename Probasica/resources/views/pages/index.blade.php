@extends('template.base')
@section('title', $page->name)
@section('slider')
    @if(Request::is('accueil') or Request::is('/'))
        @include('posts.includes.slider')
    @endif
@endsection
@section('content.h1', $page->name)
@section('content')
    @include('pages.includes.infos')
    @if(Request::is('accueil') or Request::is('/'))
        @include('works.includes.recent')
        @include('posts.includes.recent')
    @elseif(Request::is('portfolio'))
        @include('works.includes.list')
    @elseif(Request::is('blog'))
        @include('posts.includes.list')
    @elseif(Request::is('contact'))
        @include('pages.includes.contact')
    @endif
@endsection
