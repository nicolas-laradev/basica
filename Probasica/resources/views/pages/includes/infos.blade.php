@isset($page->subtitle, $page->text)
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>{{ $page->subtitle }}</h2>
                    {!! $page->text !!}
                </div>
            </div>
        </div>
    </div>
@endisset
