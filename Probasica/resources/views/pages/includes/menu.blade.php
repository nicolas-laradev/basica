@foreach ($menu as $item)
    <li @if($item->slug == Request::route('slug'))
            class="active"
        @elseif($item->slug == 'accueil' and Request::is('/'))
            class="active"
        @elseif($item->slug == 'blog' and Request::is('blog/*'))
            class="active"
        @elseif($item->slug == 'portfolio' and Request::is('portfolio/*'))
            class="active"
        @endif>
        <a href="{{ url(($item->slug == 'accueil')?"/":"/$item->slug") }}">
            {{ $item->name }}
        </a>
    </li>
@endforeach
<li><a href="{{ url('admin/dashboard') }}">Admin</a></li>
