<div class="section">
    <div class="container">
        <div class="row">
                <div class="section-title">
                    {!! $similars->isEmpty()?"<h3>Pas de projet similaires correspondants aux tags<h3>":"<h1>Projets similaires</h1>" !!}
                </div>
            <ul class="grid cs-style-2">
                @foreach($similars as $similar)
                    <div class="col-md-3 col-sm-6">
                        <figure>
                            <img src="{{ asset("uploads/img/portfolio/$similar->picture") }}" alt="{{ $similar->name }}">
                            <figcaption>
                                <h3>{{ $similar->name }}</h3>
                                <span>
                                    @forelse($similar->tags->take(2) as $tag)
                                        {{ $tag->name }}
                                    @empty
                                        Pas de tags
                                    @endforelse
                                </span>
                                <a href="{{ route('works.show', ['slug' => $similar->slug])}}">Aller voir</a>
                            </figcaption>
                        </figure>
                    </div>
                @endforeach
            </ul>
        </div>
    </div>
</div>
