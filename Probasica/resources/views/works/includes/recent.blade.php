<div class="section section-white">
    <div class="container">
        <div class="row">
            <div class="section-title">
                <h1>Nos travaux récents</h1>
            </div>
            <ul class="grid cs-style-3">
                @foreach ($works->take(6) as $work)
                    <div class="col-md-4 col-sm-6">
                        <figure>
                            <img src="{{ asset("uploads/img/portfolio/$work->picture") }}" alt="{{ $work->name }}">
                            <figcaption>
                                <h3>{{ $work->name }}</h3>
                                <span>
                                    @forelse ($work->tags->take(2) as $tag)
                                        {{ $tag->name }}
                                    @empty
                                        Pas de tag
                                    @endforelse
                                </span>
                                <a href="{{ route('works.show', ['slug' => $work->slug]) }}">Aller voir</a>
                            </figcaption>
                        </figure>
                    </div>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<hr>
