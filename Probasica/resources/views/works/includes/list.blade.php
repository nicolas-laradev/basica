<div class="section section-white" id='app'>
    <div class="container">
        <div class="row">
            <ul class="grid cs-style-3" >
                <section>
                    @foreach ($works->take(3) as $work)
                        <div class="col-md-4 col-sm-6">
                            <figure>
                                <img src="{{ asset("$work->picture") }}" alt="{{ $work->name }}">
                                <figcaption>
                                    <h3>{{ $work->name }}</h3>
                                    <span>
                                        @forelse ($work->tags->take(2) as $tag)
                                            {{ $tag->name }}
                                        @empty
                                            Pas de tag
                                        @endforelse
                                    </span>
                                    <a href="{{ route('works.show', ['slug' => $work->slug]) }}">Aller voir</a>
                                </figcaption>
                            </figure>
                        </div>
                    @endforeach
                </section>
                <transition-group name='fade' tag='div'>
                  <mon-composant class="nameItems" v-if="name.name" v-for="name in tabName"  :name="name" v-bind:key="name"></mon-composant>
                </transition-group>
            </ul>
        </div>
        <ul class="pager">
                <li><a href="#" v-on:click.prevent="test">Charger plus</a></li>
        </ul>
    </div>
</div>
<hr>
