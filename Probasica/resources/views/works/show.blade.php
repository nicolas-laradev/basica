@extends('template.base')
@section('title', $work->name)
@section('content.h1', $work->name)
@section('content')
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="product-image-large">
                        <img src="{{ asset("uploads/img/portfolio/$work->picture") }}" alt="{{ $work->name }}">
                    </div>
                    <div class="colors">
                        <span class="color-white"></span>
                        <span class="color-black"></span>
                        <span class="color-blue"></span>
                        <span class="color-orange"></span>
                        <span class="color-green"></span>
                    </div>
                </div>
                <div class="col-sm-6 product-details">
                    <h2>{{ $work->name }}</h2>
                    <h3>Description</h3>
                    <p>{!! $work->text !!}</p>
                    <h3>Détails du projet</h3>
                    <p><strong>Client: </strong>{{ $work->client }}</p>
                    <p><strong>Date: </strong>{{ $work->created_at->formatLocalized('%d %B %Y') }}</p>
                    <p><strong>Tags: </strong>
                        @forelse($work->tags as $tag)
                            {{ $tag->name }}
                        @empty
                            Pas de tags
                        @endforelse
                    </p>
                </div>
            </div>
        </div>
    </div>
    <hr>
    @include('works.includes.similars')
@endsection
