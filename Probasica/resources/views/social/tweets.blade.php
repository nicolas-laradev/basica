<div class="col-sm-6 latest-news">
    <h2>Dernières news Twitter</h2>
    @foreach ($tweets as $tweet )
        <div class="row">
            <div class="col-sm-12">
                <div class="caption"><a href={{ "https://twitter.com/NysCirentin/status/".$tweet->id }}>{{ $tweet->user->name }}</a></div>
                <div class="date">{{ strftime("%d %B %Y à %Hh%M", strtotime($tweet->created_at)) }}</div>
                <div class="intro">{{ $tweet->text }} <a href={{ "https://twitter.com/NysCirentin/status/".$tweet->id }}>Lire plus...</a></div>
            </div>
        </div>
    @endforeach
</div>
