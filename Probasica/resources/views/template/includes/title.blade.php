@if(Request::is('/') == false and Request::is('accueil') == false)
    <div class="section section-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>@yield('content.h1')</h1>
                </div>
            </div>
        </div>
    </div>
@endif
