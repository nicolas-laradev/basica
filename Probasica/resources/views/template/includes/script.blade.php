<script src="//use.edgefonts.net/bebas-neue.js"></script>
<script src="{{ asset('js/modernizr-2.6.2-respond-1.1.0.min.js') }}"></script>

<!-- Javascripts -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ asset('js/jquery-1.9.1.min.js') }}"><\/script>')</script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<!-- Scrolling Nav JavaScript -->
<script src="{{ asset('js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('js/scrolling-nav.js') }}"></script>

<script src="{{ asset('js/vue.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.3"></script>
