<!-- Bootstrap Core CSS -->
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">

<!-- Custom CSS -->
<link rel="stylesheet" href="{{ asset('css/main.css') }}">
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">

<!-- Custom Fonts & Icons -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{{ asset('css/icomoon-social.css') }}">
<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
