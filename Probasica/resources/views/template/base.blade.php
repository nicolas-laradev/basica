<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <!-- Meta -->
        @include('template.includes.meta')
        <!-- Page Title -->
        <title>Basica - @yield('title')</title>
        <!-- Style -->
        @include('template.includes.style')
    </head>
    <body>
        <!-- Page Navigation -->
        @include('template.includes.header')
        <!-- Page Header -->
        @yield('slider')
        <!-- Page Content title -->
        @include('template.includes.title')
        <!-- Page Content -->
        <div id="app">
        @yield('content')
        </div>
        <!-- Page Footer -->
        @include('template.includes.footer')
        <!-- Scripts -->
        @include('template.includes.script')
    </body>
</html>
