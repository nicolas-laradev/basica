<h4>Catégories</h4>
<ul class="blog-categories">
    @foreach($categories as $categorie)
        <li><a href="{{ route('posts.order', ['categorie' => $categorie->slug]) }}">{{ $categorie->name }}</a></li>
    @endforeach
</ul>
