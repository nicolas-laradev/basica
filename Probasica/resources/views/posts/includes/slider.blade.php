<section id="main-slider" class="no-margin">
    <div class="carousel slide">
        <ol class="carousel-indicators">
            <li data-target="#main-slider" data-slide-to="0" class="active"></li>
            <li data-target="#main-slider" data-slide-to="1"></li>
            <li data-target="#main-slider" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            @foreach($slides->take(3) as $slide)
                <div class="{{ $slides->first() == $slide ? 'item active' : 'item' }}"  style="background-image: url({{ asset("uploads/img/blog/$slide->picture") }})">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content centered">
                                    <h2 class="animation animated-item-1">{{ $slide->title }}</h2>
                                    <p class="animation animated-item-2">{!! str_limit($slide->text, $limit = 100, $end = '...') !!}</p>
                                    <br>
                                    <a class="btn btn-md animation animated-item-3" href="{{ route('posts.show', ['slug' => $slide->slug])}}">En savoir plus</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
        <i class="icon-angle-left"></i>
    </a>
    <a class="next hidden-xs" href="#main-slider" data-slide="next">
        <i class="icon-angle-right"></i>
    </a>
</section>
