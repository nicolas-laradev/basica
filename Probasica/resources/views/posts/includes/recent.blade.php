<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 featured-news">
                <h2>Derniers articles du blog</h2>
                @foreach ($posts->take(3) as $post)
                    <div class="row">
                        <div class="col-xs-4">
                            <a href="{{ route('posts.show', ['slug' => $post->slug])}}">
                                <img src="{{ asset("uploads/img/blog/$post->picture")}} " alt="{{ $post->title }}">
                            </a>
                        </div>
                        <div class="col-xs-8">
                            <div class="caption"><a href="{{ route('posts.show', ['slug' => $post->slug])}}">{{ $post->title }}</a></div>
                            <div class="date">{{ $post->created_at->formatLocalized('%d %B %Y') }}</div>
                            <div class="intro">{!! str_limit($post->text, $limit = 100, $end = '...') !!}
                                <a href="{{ route('posts.show', ['slug' => $post->slug])}}">
                                    Lire plus...
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @include('social.tweets')
        </div>
    </div>
</div>
