<h4>Articles récents</h4>
<ul class="recent-posts">
    @foreach($posts->take(4) as $post)
        <li><a href="{{ route('posts.show', ['slug' => $post->slug])}}">{{ $post->title }}</a></li>
    @endforeach
</ul>
@include('categories.list')
