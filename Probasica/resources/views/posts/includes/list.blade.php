<div class="section">
    <div class="container">
        <div class="row">
          <transition-group name='fade' tag='div'>
          <pagination  v-for="element in elements" :element="element" v-bind:key="element"></pagination>
        </transition-group>
            @foreach($posts as $post)
                <div class="col-sm-6">
                    <div class="blog-post blog-single-post">
                        <div class="single-post-title">
                            <h2>{{ $post->title }}</h2>
                        </div>
                        <div class="single-post-image">
                            <img src="{{ asset("uploads/img/blog/$post->picture") }}" alt="{{ $post->title }}" width='500' height='300'>
                        </div>
                        <div class="single-post-info">
                            <i class="glyphicon glyphicon-time"></i>Publié le {{ $post->created_at->formatLocalized('%d %B %Y') }}
                        </div>
                        <div class="single-post-content">
                            <p>{!! str_limit($post->text, $limit = 100, $end = '...') !!}</p>
                            <a href="{{ route('posts.show', ['slug' => $post->slug])}}" class="btn">Lire plus...</a>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="pagination-wrapper ">
                <ul class="pagination pagination-sm">
                    <li v-on:click.prevent='pagination'>{{ $posts->links() }}</li>
                </ul>
            </div>
        </div>
    </div>
</div>
