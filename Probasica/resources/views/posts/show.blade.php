@extends('template.base')
@section('title', $post->title)
@section('content.h1', $post->title)
@section('content')
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="blog-post blog-single-post">
                        <div class="single-post-title">
                            <h2>{{ $post->title }}</h2>
                        </div>
                        <div class="single-post-image">
                            <img src="{{ asset("uploads/img/blog/$post->picture") }}" alt="{{ $post->title }}">
                        </div>
                        <div class="single-post-info">
                            <i class="glyphicon glyphicon-time"></i>{{ $post->created_at->formatLocalized('%d %B %Y') }}
                        </div>
                        <div class="single-post-content">
                            {!! $post->text !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 blog-sidebar">
                    @include('posts.includes.sidebar')
                </div>
            </div>
        </div>
    </div>
@endsection
