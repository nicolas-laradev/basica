@extends('template.base')
@section('title', "Catégorie : $cat->name")
@section('content.h1', "Catégorie : $cat->name")
@section('content')
    @if(count($posts) > 0)
        @include('posts.includes.list')
    @else
        <div class="section">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-12">
    					<h1>Aucun article dans cette catégorie</h1>
    				</div>
    			</div>
    		</div>
    	</div>
    @endif
@endsection
