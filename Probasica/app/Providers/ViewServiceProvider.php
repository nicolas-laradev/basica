<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

use App\Http\Views\Pages\IndexComposer;
use App\Http\Views\Pages\MenuComposer;

use App\Http\Views\Posts\ShowComposer as PostShowComposer;
use App\Http\Views\Posts\ListComposer;
use App\Http\Views\Posts\RecentComposer;
use App\Http\Views\Posts\SliderComposer;
use App\Http\Views\Posts\SidebarComposer;
use App\Http\Views\Posts\OrderComposer;


use App\Http\Views\Works\ShowComposer as WorkShowComposer;
use App\Http\Views\Works\SimilarsComposer;
use App\Http\Views\Works\RecentListComposer;

use App\Http\Views\Social\TweetsComposer;


class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', MenuComposer::class);
        View::composer('pages.index', IndexComposer::class);
        View::composer('posts.includes.slider', SliderComposer::class);
        View::composer('social.tweets', TweetsComposer::class);
        View::composer('posts.includes.sidebar', SidebarComposer::class);
        View::composer('posts.show', PostShowComposer::class);
        View::composer('posts.includes.list', ListComposer::class);
        View::composer('posts.order', OrderComposer::class);
        View::composer('posts.includes.recent', RecentComposer::class);
        View::composer('works.show', WorkShowComposer::class);
        View::composer('works.includes.similars', SimilarsComposer::class);
        View::composer(
            ['works.includes.recent', 'works.includes.list'],
            RecentListComposer::class
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
