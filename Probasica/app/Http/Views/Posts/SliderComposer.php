<?php
/**
 * Composeur de vue SliderComposer affectant la vue posts/includes/slider.blade.php
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Views\Posts;

use Illuminate\View\View;
use Illuminate\Support\Facades\Request;
use App\Repositories\PostsRepository;

/**
 * Définition de la classe SliderComposer qui alloue les variables des vues concernant les posts du slider
 */
class SliderComposer
{
    /**
     * Implémentation du repository des posts
     *
     * @var PostsRepository
     */
    protected $posts;

    /**
     * Constructeur du composer des posts en slider
     *
     * @param PostsRepository $posts
     * @return void
     */
    public function __construct(PostsRepository $posts)
    {
        $this->posts = $posts;
    }

    /**
     * Récupération des posts dont l'id de la catégorie vaut 0
     * Allocation des variables à la vue
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $slides = $this->posts->findByCategorie(0);
        $view->with(compact('slides'));
    }
}
