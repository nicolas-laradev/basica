<?php
/**
 * Composeur de vue SidebarComposer affectant la vue posts/includes/sidebar.blade.php
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Views\Posts;

use Illuminate\View\View;
use Illuminate\Support\Facades\Request;
use App\Repositories\PostsRepository;
use App\Repositories\CategoriesRepository;

/**
 * Définition de la classe SidebarComposer qui alloue les variables des vues concernant la liste des posts en sidebar
 */
class SidebarComposer
{
    /**
     * Implémentation du repository des posts
     *
     * @var PostsRepository
     */
    protected $posts;
    protected $categories;
    /**
     * Constructeur du composer des posts en sidebar
     *
     * @param PostsRepository $posts
     * @return void
     */
    public function __construct(PostsRepository $posts, CategoriesRepository $categories)
    {
        $this->posts = $posts;
        $this->categories = $categories;
    }

    /**
     * Récupération des posts par ordre de date décroissante stockés dans une variable
     * Récupération des catégories stockées dans une variable
     * Allocation des variables à la vue
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Request::is('blog/*') and Request::is('blog/order/*') == false){
            $posts = $this->posts->orderByDateDesc()->where('slug', '!=', Request::route('slug'));
            $categories = $this->categories->findAll();
        }
        $view->with(compact('posts', 'categories'));
    }
}
