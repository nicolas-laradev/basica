<?php
/**
 * Composeur de vue RecentComposer affectant la vue posts/includes/recent.blade.php
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Views\Posts;

use Illuminate\View\View;
use Illuminate\Support\Facades\Request;
use App\Repositories\PostsRepository;

/**
 * Définition de la classe RecentComposer qui alloue les variables des vues concernant les posts récents
 */
class RecentComposer
{
    /**
     * Implémentation du repository des posts
     *
     * @var PostsRepository
     */
    protected $posts;

    /**
     * Constructeur du composer des posts récents
     *
     * @param PostsRepository $posts
     * @return void
     */
    public function __construct(PostsRepository $posts)
    {
        $this->posts = $posts;
    }

    /**
     * Récupération de la liste des posts triés par ordre décroissant selon la date de création
     * Allocation des variables à la vue
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $posts = $this->posts->orderByDateDesc();
        $view->with(compact('posts'));
    }
}
