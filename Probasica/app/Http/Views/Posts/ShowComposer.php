<?php
/**
 * Composeur de vue ShowComposer affectant la vue posts/show.blade.php
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Views\Posts;

use Illuminate\View\View;
use Illuminate\Support\Facades\Request;
use App\Repositories\PostsRepository;

/**
 * Définition de la classe ShowComposer qui alloue les variables des vues concernant les détails des posts
 */
class ShowComposer
{
    /**
     * Implémentation du repository des posts
     *
     * @var PostsRepository
     */

    protected $posts;
    /**
     * Constructeur du composer des détails de posts
     *
     * @param PostsRepository $posts
     * @return void
     */
    public function __construct(PostsRepository $posts)
    {
        $this->posts = $posts;
    }

    /**
     * Récupération des données d'un élément choisis selon le slug envoyé par la route
     * Allocation des variables à la vue
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Request::is('blog/*') and Request::is('blog/order/*') == false){
            $post = $this->posts->findOneBySlug(Request::route('slug'));
        }
        $view->with(compact('post'));
    }
}
