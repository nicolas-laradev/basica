<?php
/**
 * Composeur de vue ListComposer affectant la vue posts/includes/list.blade.php
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Views\Posts;

use Illuminate\View\View;
use Illuminate\Support\Facades\Request;
use App\Repositories\PostsRepository;

/**
 * Définition de la classe ListComposer qui alloue les variables des vues concernant la liste des posts
 */
class ListComposer
{
    /**
     * Implémentation du repository des posts
     *
     * @var PostsRepository
     */
    protected $posts;

    /**
     * Constructeur du composer de la liste des posts
     *
     * @param PostsRepository $posts
     * @return void
     */
    public function __construct(PostsRepository $posts)
    {
        $this->posts = $posts;
    }

    /**
     * Récupération de la liste des posts triés par ordre décroissant selon la date de création
     * La liste est paginée
     * Allocation des variables à la vue
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Request::is('blog')){
            $posts = $this->posts->paginateByDateDesc();
        }
        $view->with(compact('posts'));
    }
}
