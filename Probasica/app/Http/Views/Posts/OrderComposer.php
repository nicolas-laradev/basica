<?php
/**
 * Composeur de vue OrderComposer affectant la vue posts/order.blade.php
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Views\Posts;

use Illuminate\View\View;
use Illuminate\Support\Facades\Request;
use App\Repositories\PostsRepository;
use App\Repositories\CategoriesRepository;

/**
 * Définition de la classe OrderComposer qui alloue les variables des vues concernant les posts triés selon la catégorie
 */
class OrderComposer
{
    /**
     * Implémentation du repository des posts
     *
     * @var PostsRepository
     */
    protected $posts;

    /**
     * Implémentqtion du repository des categories
     *
     * @var CategoriesRepository
     */
    protected $categories;
    /**
     * Constructeur du composer des posts triés selon la catégorie
     *
     * @param PostsRepository $posts
     * @return void
     */
    public function __construct(PostsRepository $posts, CategoriesRepository $categories)
    {
        $this->posts = $posts;
        $this->categories = $categories;
    }

    /**
     * Récupération de la catégorie selon le slugCat envoyé par la route
     * Récupération des posts triés selon la catégories en question
     * Allocation des variables à la vue
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Request::is('blog/order/*')){
            $cat = $this->categories->findOneBySlug(Request::route('slugCat'));
            $posts = $this->posts->paginateByCategorie($cat->id);
        }
        $view->with(compact('posts', 'cat'));
    }
}
