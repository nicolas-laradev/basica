<?php
/**
 * Composeur de vue MenuComposer affectant toutes les vues
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Views\Pages;

use Illuminate\View\View;
use App\Repositories\PagesRepository;

/**
 * Définition de la classe MenuComposer qui alloue les variables des vues concernant le menu des pages
 */
class MenuComposer
{
    /**
     * Implémentation du repository des pages
     *
     * @var PagesRepository
     */
    protected $pages;

    /**
     * Constructeur du composer du menu
     *
     * @param PagesRepository $pages
     * @return void
     */
    public function __construct(PagesRepository $pages)
    {
        $this->pages = $pages;
    }

    /**
     * Récupération de toutes les pages dans une variable
     * Allocation des variables à la vue
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $menu = $this->pages->findAll();
        $view->with(compact('menu'));
    }
}
