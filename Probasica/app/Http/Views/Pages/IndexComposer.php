<?php
/**
 * Composeur de vue IndexComposer affectant la vue pages/index.blade.php
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Views\Pages;

use Illuminate\View\View;
use App\Repositories\PagesRepository;
use Illuminate\Support\Facades\Request;

/**
 * Définition de la classe IndexComposer qui alloue les variables des vues concernant le détail des pages
 */
class IndexComposer
{
    /**
     * Implémentation du repository des pages
     *
     * @var PagesRepository
     */
    protected $pages;

    /**
     * Constructeur du composer du détail des pages
     *
     * @param PagesRepository $pages
     * @return void
     */
    public function __construct(PagesRepository $pages)
    {
        $this->pages = $pages;
    }

    /**
     * Récupération du contenu des pages en fonction du paramètre 'slug' de la route page.index
     * Attribution d'un $slug par défault 'accueil' si le pattern de la route est '/'.
     * Allocation des variables à la vue
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $slug = Request::is('/') ? 'accueil' : Request::route('slug');
        $page = $this->pages->findOneBySlug($slug);
        $view->with(compact('page'));
    }
}
