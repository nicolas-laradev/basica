<?php
/**
 * Composeur de vue SimilarsComposer affectant la vue works/includes/similars.blade.php
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Views\Works;

use Illuminate\View\View;
use Illuminate\Support\Facades\Request;
use App\Repositories\WorksRepository;

/**
 * Définition de la classe SimilarsComposer qui alloue les variables des vues concernant les works similaires
 */
class SimilarsComposer
{
    /**
     * Implémentation du repository des works
     *
     * @var WorksRepository
     */
    protected $works;

    /**
     * Constructeur du composer des works similaires
     *
     * @param WorksRepository $works
     * @return void
     */
    public function __construct(WorksRepository $works)
    {
        $this->works = $works;
    }

    /**
     * Récupération du work de la page courante
     * Récupration de la liste des tags de ce work_id
     * Récupration des works ayant au mois un tags similaires à la liste précedemment récupérée
     * Allocation des variables à la vue
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Request::is('portfolio/*')){
            $work = $this->works->findOneBySlug(Request::route('slug'));
            $tags = $work->find($work->id)->tags->pluck('name')->all();
            $similars = $this->works->randWithAnyTags($tags, $work->slug);
        }
        $view->with(compact('similars'));
    }
}
