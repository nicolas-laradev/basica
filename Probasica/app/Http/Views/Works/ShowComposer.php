<?php
/**
 * Composeur de vue ShowComposer affectant la vue works/show.blade.php
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Views\Works;

use Illuminate\View\View;
use Illuminate\Support\Facades\Request;
use App\Repositories\WorksRepository;

/**
 * Définition de la classe ShowComposer qui alloue les variables des vues concernant les details des works
 */
class ShowComposer
{
    /**
     * Implémentation du repository des works
     *
     * @var WorksRepository
     */
    protected $works;

    /**
     * Constructeur du composer des details des works
     *
     * @param WorksRepository $works
     * @return void
     */
    public function __construct(WorksRepository $works)
    {
        $this->works = $works;
    }

    /**
     * Récupération des données d'un élement selon le slug envoyé par la route
     * Allocation des variables à la vue
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Request::is('portfolio/*')){
            $work = $this->works->findOneBySlug(Request::route('slug'));
        }
        $view->with(compact('work'));
    }
}
