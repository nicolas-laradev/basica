<?php
/**
 * Composeur de vue RecentListComposer affectant les vues works/includes/recent.blade.php et works/includes/list.blade.php
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Views\Works;

use Illuminate\View\View;
use Illuminate\Support\Facades\Request;
use App\Repositories\WorksRepository;

/**
 * Définition de la classe RecentListComposer qui alloue les variables des vues concernant les works
 */
class RecentListComposer
{
    /**
     * Implémentation du repository des works
     *
     * @var WorksRepository
     */
    protected $works;

    /**
     * Constructeur du composer des works
     *
     * @param WorksRepository $works
     * @return void
     */
    public function __construct(WorksRepository $works)
    {
        $this->works = $works;
    }

    /**
     * Récupération des works trié par ordre décroissant selon la date de création
     * Allocation des variables à la vue
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $works = $this->works->orderByDateDesc();
        $view->with(compact('works'));
    }
}
