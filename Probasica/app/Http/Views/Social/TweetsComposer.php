<?php
/**
 * Composeur de vue TweetsComposer affectant la vue social/tweets.blade.php
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Views\Social;

use Illuminate\View\View;
use App\Repositories\TweetsRepository;

/**
 * Définition de la classe TweetsComposer qui alloue les variables des vues concernant les tweets
 */
class TweetsComposer
{
    /**
     * Implémentation du repository des tweets
     *
     * @var TweetsRepository
     */
    protected $tweets;

    /**
     * Constructeur du composer des tweets
     *
     * @param TweetsRepository $tweets
     * @return void
     */
    public function __construct(TweetsRepository $tweets)
    {
        $this->tweets = $tweets;
    }

    /**
     * Allocation des variables à la vue
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $tweets = $this->tweets->findThreeLasts();
        $view->with(compact('tweets'));
    }
}
