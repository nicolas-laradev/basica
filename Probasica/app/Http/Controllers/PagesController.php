<?php
/**
 * Contrôleur PagesController
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Controllers;

/**
 * Définition de la classe PagesController qui gère ce qui touche aux éléments du modèle Page
 */
class PagesController extends Controller
{
    /**
     * Méthode index() retournant la vue pages/index.blade.php
     * @return view
     */
    public function index()
    {
        return view('pages.index');
    }
}
