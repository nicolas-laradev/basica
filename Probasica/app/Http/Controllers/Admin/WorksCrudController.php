<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\WorksEditRequest as UpdateRequest;
use App\Http\Requests\WorksStoreRequest as StoreRequest;

class WorksCrudController extends CrudController {

	/**
	 * Mise en place du tableau et des formulaires CRUD
	 * @return void
	 */
	public function setup() {
        $this->crud->setModel("App\Http\Models\Work");
        $this->crud->setRoute("admin/works");
        $this->crud->setEntityNameStrings('travail', 'travaux');

        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Nom du travail',
        ]);
		$this->crud->addColumn([
            'label' => "Tags",
            'type' => 'select_multiple',
        	'name' => 'tags',
            'entity' => 'tags',
            'attribute' => 'name',
            'model' => 'App\Http\Models\Tag'
        ]);
		$this->crud->addColumn([
            'name' => 'client',
            'label'=> 'Client',
        ]);
        $this->crud->addColumn([
            'name' => 'created_at',
            'label' => 'Date de création',
			'type' => 'datetime',
        ]);
		$this->crud->addColumn([
            'name' => 'updated_at',
            'label' => 'Dernières modifications',
			'type' => 'datetime',
        ]);
        $this->crud->addField([
        	'name' => 'name',
        	'label' => "Nom du travail",
        ]);
		$this->crud->addField([
			'name' => 'picture',
            'label' => 'Image',
			'filename' => "image_filename",
            'type' => 'image',
			'crop' => true,
    		'aspect_ratio' => 1.66,
			'prefix' => 'uploads/img/portfolio/',
        ]);
		$this->crud->addField([
        	'name' => 'text',
        	'label' => "Texte",
            'type' => 'ckeditor',
    	]);
        $this->crud->addField([
            'label' => "Tags",
            'type' => 'select2_multiple',
            'name' => 'tags',
            'entity' => 'tags',
            'attribute' => 'name',
            'model' => "App\Http\Models\Tag",
            'pivot' => true,
    	]);
		$this->crud->addField([
            'name' => 'client',
            'label'=> 'Client',
        ]);
    }

	/**
	 * Fonction store qui gère l'ajout de posts
	 * @param  StoreRequest $request
	 * @return parent::storeCrud();
	 */
	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	/**
	 * Fonction store qui gère l'edition de posts
	 * @param  UpdateRequest $request
	 * @return parent::updateCrud();
	 */
	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
