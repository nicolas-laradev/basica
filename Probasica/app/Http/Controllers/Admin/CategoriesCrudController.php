<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\CategoriesCrudRequest as UpdateRequest;
use App\Http\Requests\CategoriesCrudRequest as StoreRequest;

class CategoriesCrudController extends CrudController {

	/**
	 * Mise en place du tableau et des formulaires CRUD
	 * @return void
	 */
	public function setup() {
        $this->crud->setModel("App\Http\Models\Categorie");
        $this->crud->setRoute("admin/categories");
        $this->crud->setEntityNameStrings('categorie', 'categories');

        $this->crud->addColumn([
            'name' =>'name',
            'label'=>'Nom',
        ]);
        $this->crud->addColumn([
            'name' =>'created_at',
            'label'=>'Date de création',
			'type' => 'datetime',
        ]);
		$this->crud->addColumn([
            'name' =>'updated_at',
            'label'=>'Dernières modifications',
			'type' => 'datetime',
        ]);
        $this->crud->addField([
        	'name' => 'name',
        	'label' => "Nom",
        ]);
    }

	/**
	 * Fonction store qui gère l'ajout de catégorie
	 * @param  StoreRequest $request
	 * @return parent::storeCrud();
	 */
	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	/**
	 * Fonction update qui gère l'edition de catégorie
	 * @param  UpdateRequest $request
	 * @return parent::storeCrud();
	 */
	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
	
	/**
	 * Surcharge de la fonction destory qui gère la suppression de catégorie
	 * @param  integer $id
	 * @return abort or delete
	 */
	public function destroy($id)
	{
		$this->crud->hasAccessOrFail('delete');
		return $id == 0 ? abort(405) : $this->crud->delete($id);
	}
}
