<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\PostsEditRequest as UpdateRequest;
use App\Http\Requests\PostsStoreRequest as StoreRequest;

class PostsCrudController extends CrudController {

	/**
	 * Mise en place du tableau et des formulaires CRUD
	 * @return void
	 */
	public function setup() {
        $this->crud->setModel('App\Http\Models\Post');
        $this->crud->setRoute("admin/posts");
        $this->crud->setEntityNameStrings('article', 'articles');

        $this->crud->addColumn([
            'name' =>'title',
            'label'=>'Titre',
        ]);
		$this->crud->addColumn([
            'label' => "Catégorie",
            'type' => 'select',
        	'name' => 'categorie',
            'entity' => 'categories',
            'attribute' => 'name',
            'model' => 'App\Http\Models\Categorie'
        ]);
		$this->crud->addColumn([
            'name' =>'created_at',
            'label'=>'Date de création',
			'type' => 'datetime',
        ]);
		$this->crud->addColumn([
            'name' =>'updated_at',
            'label'=>'Dernières modifications',
			'type' => 'datetime',
        ]);
        $this->crud->addField([
        	'name' => 'title',
        	'label' => "Titre",
        ]);
		$this->crud->addField([
            'name' => 'picture',
            'label' => 'Image',
			'filename' => "image_filename",
            'type' => 'image',
			'crop' => true,
    		'aspect_ratio' => 1.66,
			'prefix' => 'uploads/img/blog/',
        ]);
		$this->crud->addField([
        	'name' => 'text',
        	'label' => "Texte",
			'type' => 'ckeditor',
        ]);
        $this->crud->addField([
            'label' => "Catégorie",
            'type' => 'select2',
        	'name' => 'categorie',
            'entity' => 'categories',
            'attribute' => 'name',
            'model' => 'App\Http\Models\Categorie'
    	]);
		$this->crud->addField([
        	'name' => 'title',
        	'label' => "Titre",
        ]);
    }

	/**
	 * Fonction store qui gère l'ajout de posts
	 * @param  StoreRequest $request
	 * @return parent::storeCrud();
	 */
	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	/**
	 * Fonction store qui gère l'edition de posts
	 * @param  UpdateRequest $request
	 * @return parent::updateCrud();
	 */
	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}

}
