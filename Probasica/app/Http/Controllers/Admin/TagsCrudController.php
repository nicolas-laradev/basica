<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\TagsCrudRequest as UpdateRequest;
use App\Http\Requests\TagsCrudRequest as StoreRequest;

class TagsCrudController extends CrudController {

	/**
	 * Mise en place du tableau et des formulaires CRUD
	 * @return void
	 */
	public function setup() {
        $this->crud->setModel("App\Http\Models\Tag");
        $this->crud->setRoute("admin/tags");
        $this->crud->setEntityNameStrings('tag', 'tags');

        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Nom',
        ]);
        $this->crud->addColumn([
            'name' => 'created_at',
            'label' => 'Date de création',
			'type' => 'datetime',
        ]);
		$this->crud->addColumn([
            'name' => 'updated_at',
            'label' => 'Dernières modifications',
			'type' => 'datetime',
        ]);
        $this->crud->addField([
        	'name' => 'name',
        	'label' => "Nom",
        ]);
    }

	/**
	 * Fonction store qui gère l'ajout de posts
	 * @param  StoreRequest $request
	 * @return parent::storeCrud();
	 */
	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	/**
	 * Fonction store qui gère l'edition de posts
	 * @param  UpdateRequest $request
	 * @return parent::updateCrud();
	 */
	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
