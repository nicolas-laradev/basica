<?php
/**
 * Contrôleur PostsController
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Controllers;

/**
 * Définition de la classe PostsController qui gère ce qui touche aux éléments du modèle Post
 */
class PostsController extends Controller
{
    /**
     * Méthode show() retournant la vue posts/show.blade.php
     * @return view
     */
    public function show()
    {
        return view('posts.show');
    }

    /**
     * Méthode order() retournant la vue posts/order.blade.php
     * @return view
     */
    public function order()
    {
        return view('posts.order');
    }
}
