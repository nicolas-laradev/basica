<?php
/**
 * Contrôleur WorksController
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Controllers;

/**
 * Définition de la classe WorksController qui gère ce qui touche aux éléments de la table 'works'
 */
class WorksController extends Controller
{
    /**
     * Méthode show() retournant la vue works/show.blade.php
     * @return view
     */
    public function show()
    {
        return view('works.show');
    }
}
