<?php
/**
 * Modèle Work gérant les éléments de la table 'works'
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

/**
 * class Work gérant les éléments de la table 'works'
 * @version 1.0.0
 */
class Work extends Model
{
    use \Spatie\Tags\HasTags;
    use CrudTrait;
    use Sluggable;

    /**
     * Définition de la table gérée par ce modèle
     *
     * @var string
     */
    protected $table = 'works';
    /**
     * Ces attributs seront les seuls assignables
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'text', 'picture', 'client',
    ];

    /**
     * Définition de la liaison ManyToMany entre Work et Tag
     * @return method
     */
    public function tags(){
        return $this->belongsToMany('App\Http\Models\Tag', 'taggables', 'work_id', 'tag_id');
    }

    public static function boot()
    {
        parent::boot();
        // En cas de delete simple, cette fonction est nécessaire pour supprimer également le fichier du disque
        static::deleting(function($obj) {
            Storage::disk('uploads_portfolio')->delete($obj->picture);
        });
    }

    public function setPictureAttribute($value)
    {
        $attribute_name = "picture";
        $disk = "uploads_portfolio";
        $request = new Request();

        if ($value != old($value)) {
            if(starts_with($value, 'data:image')){
                // Suppression de l'ancienne image destinée à être remplacée
                Storage::disk($disk)->delete($this->picture);
                // Annulation du champ ciblé dans la base de donnée correspondant au chemin de l'ancienne image
                $this->attributes[$attribute_name] = null;
                // Récupération de la valeur du nom du fichier choisi
                $filename = $request->input('image_filename');
                // Si le fichier possède bel et bien un nom, on l'encode en uniqid pour le rendre unique
                // Ensuite, on en profite pour le convertir en PNG pour uniformisé le type d'image du site
                if ($filename !== ''){
                    $filename = uniqid('img').'.png';
        		}
                // Création d'une instance de la classe Image pour préparer l'upload de la nouvelle image
                $image = Image::make($value);
                // Upload de la nouvelle image sur le disque
                Storage::disk($disk)->put($filename, $image->stream());
                // Sauvegarde du chemin pour l'ajouter dans la base de donnée
                $this->attributes[$attribute_name] = $filename;
            }
        }
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
                'onUpdate' => true,
            ],
        ];
    }
}
