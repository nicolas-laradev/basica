<?php
/**
 * Modèle User gérant les éléments de la table 'users'
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * class User gérant les éléments de la table 'users'
 * @version 1.0.0
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * Ces attributs seront les seuls assignables
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * Ces attributes seront cachés dans les formulaires.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
