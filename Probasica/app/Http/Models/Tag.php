<?php
/**
 * Modèle Tags gérant les éléments de la table 'tags' (surcharge de la classe homonyme du package spatie/laravel-tags)
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * class Tag gérant les éléments de la table 'tags'
 * @version 1.0.0
 */
class Tag extends Model
{
    use CrudTrait;
    use Sluggable;

    protected $fillable = ['name'];
    /**
     * Surcharge pour changer la classe de gestion des Tags (initalement géré par le package spatie/laravel-tags)
     *
     * @return Tag::class;
     */
    public static function getTagClassName(): string
    {
        return Tag::class;
    }
    /**
     * Définition de la liaison ManyToMany entre Tag et Work
     * @return method
     */
    public function works(){
        return $this->belongsToMany('App\Http\Models\Work', 'taggables', 'work_id', 'tag_id');
    }
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
                'onUpdate' => true,
            ],
        ];
    }
}
