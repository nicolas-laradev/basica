<?php
/**
 * Modèle Page gérant les éléments de la table 'pages'
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * class Page gérant les éléments de la table 'pages'
 * @version 1.0.0
 */
class Page extends Model
{
    /**
     * Définition de la table gérée par ce modèle
     * @var string
     */
    protected $table = 'pages';

    /**
     * Choix de l'extension des dates de création et de modification
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Ces attributs seront les seuls assignables
     * @var array
     */
    protected $fillable = [
        'name', 'title', 'subtitle', 'slug', 'text',
    ];
}
