<?php
/**
 * Modèle Categorie gérant les éléments de la table 'categories'
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;


/**
 * class Categorie gérant les éléments de la table 'categories'
 * @version 1.0.0
 */
class Categorie extends Model
{
    use CrudTrait;
    use Sluggable;
    /**
     * Définition de la table gérée par ce modèle
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * Ces attributs seront les seuls assignables
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Définition de la liaison OneToMany entre Post et Categorie
     * @return method
     */
    public function posts()
    {
        return $this->hasMany('App\Http\Models\Post');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
                'onUpdate' => true,
            ],
        ];
    }
}
