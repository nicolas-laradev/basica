<?php
/**
 * Repository des PagesRepository
 *
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Repositories;

use App\Http\Models\Page;

/**
 * Définition de la classe PagesRepository qui gère les éléments de la table pages
 * Les méthodes créées dans cette classe seront réutilisables (exemple : dans les contrôleurs et les composeurs de vue)
 */
class PagesRepository
{
    /**
     * Instanciation du modèle Page
     *
     * @var Page
     */
    protected $pages;

    /**
     * Constructeur du repository des pages
     *
     * @param Page $pages
     * @return void
     */
    public function __construct(Page $pages)
    {
        $this->pages = $pages;
    }

    /**
     * Sélectionne tous les éléments de la table pages
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->pages->all();
    }

    /**
     * Méthode findOneBySlug qui récupère un éléments de la table pages selon le slug
     *
     * @param string $slug
     * @return object
     */
    public function findOneBySlug($slug)
    {
        return $this->pages->whereSlug($slug)->firstOrFail();
    }
}
