<?php
/**
 * Repository des PostsRepository
 *
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Repositories;

use App\Http\Models\Post;

/**
 * Définition de la classe PostsRepository qui gère les éléments de la table posts
 * Les méthodes créées dans cette classe seront réutilisables (exemple : dans les contrôleurs et les composeurs de vue)
 */
class PostsRepository
{
    /**
     * Instanciation du modèle Post
     *
     * @var Post
     */
    protected $post;

    /**
     * Constructeur du repository des posts
     *
     * @param  Posts $posts
     * @return void
     */
    public function __construct(Post $posts)
    {
        $this->posts = $posts;
    }

    /**
     * Méthode orderByDateDesc() qui récupère tous les éléments de la tables posts par ordre décroissant de date de création
     *
     * @return mixed
     */
    public function orderByDateDesc()
    {
        return $this->posts->orderByDesc('created_at')->get();
    }

    /**
     * Méthode paginateByDateDesc() qui récupère tous les éléments de la tables posts par ordre décroissant de date de création.
     * Cette fois, une pagination est anticipée.
     *
     * @return mixed
     */
    public function paginateByDateDesc()
    {
        return $this->posts->orderByDesc('created_at')->paginate(6);
    }

    /**
     * Méthode findByCategorie() qui récupère tous les éléments de la tables posts selon l'id d'un élément de la table categories
     * Liaison 1-n
     * @return mixed
     */
    public function findByCategorie($categorie)
    {
         return $this->posts->whereCategorie($categorie)->get();
    }

    /**
     * Méthode findOneBySlug() qui récupère un élément de la table posts selon le slug
     *
     * @param  string $slug
     * @return object
     */
    public function findOneBySlug($slug)
    {
        return $this->posts->whereSlug($slug)->firstOrFail();
    }

    /**
     * Méthode paginateByCategorie qui récupère tous les éléments de la table posts selon l'id d'un élément de la table categories
     * Tri par ordre de date décroissante
     * Pagination active
     * Liason 1-n
     *
     * @param  integer $categorie
     * @return mixed
     */
    public function paginateByCategorie($categorie)
    {
        return $this->posts->with('categories')->whereCategorie($categorie)->orderByDesc('created_at')->paginate(6);
    }
}
