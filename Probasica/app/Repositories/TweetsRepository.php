<?php
/**
 * Repository des TweetsRepository
 *
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Repositories;

use Twitter;

/**
 * Définition de la classe PostsRepository qui gère les éléments récupéré avec l'API Twitter
 * Les méthodes créées dans cette classe seront réutilisables (exemple : dans les contrôleurs et les composeurs de vue)
 */
class TweetsRepository
{
    /**
     * Instanciation de la classe Twitter
     *
     * @var Twitter
     */
    protected $tweets;

    /**
     * Constructeur du repository des tweets
     *
     * @param Twitter $tweets
     * @return void
     */
    public function __construct(Twitter $tweets)
    {
        $this->tweets = $tweets;
    }

    /**
     * Méthode findThreeLasts() qui récupère les 3 derniers tweets d'un compte Twitter spécifique
     *
     * @return mixed
     */
    public function findThreeLasts()
    {
        return $this->tweets::getUserTimeline(['count'=>3,'format'=>'object']);
    }

}
