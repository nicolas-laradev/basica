<?php
/**
 * Repository des WorksRepository
 *
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */

namespace App\Repositories;

use App\Http\Models\Work;

/**
 * Définition de la classe WorksRepository qui gère les éléments de la table works
 * Les méthodes créées dans cette classe seront réutilisables (exemple : dans les contrôleurs et les composeurs de vue)
 */
class WorksRepository
{
    /**
     * Instanciation du modèle Work
     *
     * @var Work
     */
    protected $works;
    /**
     * Constructeur du repository des works
     *
     * @param Works $works
     * @return void
     */
    public function __construct(Work $works)
    {
        $this->works = $works;
    }

    /**
     * Methode orderByDateDesc() qui récupère tous les éléments de la table works par ordre décroissant de date de création
     *
     * @return mixed
     */
    public function orderByDateDesc()
    {
        return $this->works->orderByDesc('created_at')->get();
    }

    /**
     * Méthode findOneBySlug() qui récupère un élément de table works selon le slug
     *
     * @param  string $slug
     * @return object
     */
    public function findOneBySlug($slug)
    {
        return $this->works->with('tags')->whereSlug($slug)->firstOrFail();
    }

    /**
     * Méthode randWithAnyTags qui récupère aléatoirement 4 éléments de la tables posts uniquement si ils ont au moins un tag
     * similaire à un tag de l'élément affiché sur le page de détail
     *
     * @param  string $tags (Tag)
     * @param  string $slug (Post)
     * @return mixed
     */
    public function randWithAnyTags($tags, $slug)
    {
        $work = $this->findOneBySlug($slug);
        return $this->works::withAnyTags($tags)->where('id', '!=', $work->id)->inRandomOrder()->take(4)->get();
    }
}
