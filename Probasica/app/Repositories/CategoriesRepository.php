<?php
/**
 * Repository des CategoriesRepository
 *
 * @author Nys Corentin & Lamy Nicolas
 * @version 1.0.0
 */
namespace App\Repositories;

use App\Http\Models\Categorie;

/**
 * Définition de la classe CategoriesRepository qui gère les éléments de la table catégories
 * Les méthodes créées dans cette classe seront réutilisables (exemple : dans les contrôleurs et les composeurs de vue)
 */
class CategoriesRepository
{
    /**
     * Instanciation du modèle Catégorie
     *
     * @var Categorie
     */
    protected $categories;

    /**
     * Constructeur du repository des categories
     *
     * @param Categorie $categories
     * @return void
     */
    public function __construct(Categorie $categories)
    {
        $this->categories = $categories;
    }

    /**
     * Méthode findAll() qui récupère tous les éléments de la table categories
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->categories->where('id', '!=', 0)->get();
    }

    /**
     * Méthode findOneBySlug qui récupère un éléments de la table categories selon le slug
     *
     * @param  string $slug
     * @return object
     */
    public function findOneBySlug($slug){
        return $this->categories->whereSlug($slug)->firstOrFail();
    }

}
