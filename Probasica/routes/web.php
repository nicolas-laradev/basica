<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/'], function () {
    Route::name('index')->get('/', 'PagesController@index');
    Route::name('pages.index')->get('{slug}', 'PagesController@index');
});

Route::group(['prefix' => 'portfolio'], function () {
    Route::name('works.show')->get('{slug}', 'WorksController@show');
    Route::name('works.more')->post('more','WorksController@more');
});

Route::group(['prefix' => 'blog'], function () {
    Route::name('posts.show')->get('{slug}', 'PostsController@show');
    Route::name('posts.order')->get('/order/{slugCat}', 'PostsController@order');
});
